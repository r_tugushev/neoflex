/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.MessageDriven;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 *
 * @author rushan
 */
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "clientId", propertyValue = "MessageDrivenBean"),
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/Destination1"),
    @ActivationConfigProperty(propertyName = "subscriptionDurability", propertyValue = "Durable"),
    @ActivationConfigProperty(propertyName = "subscriptionName", propertyValue = "MessageDrivenBean"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class MessageDrivenBean implements MessageListener{

    @Resource(mappedName="jms/ConnectionFactory1")
    private ConnectionFactory connectionFactory1;

    @Resource(mappedName="jms/Destination1")
    private Queue queue1;
            
    @Resource(mappedName="jms/ConnectionFactory2")
    private ConnectionFactory connectionFactory2;

    @Resource(mappedName="jms/Destination2")
    private Queue queue2;
                   
    public MessageDrivenBean(){}
    
    @Override
    public void onMessage(Message message) {
            try {
                Connection connection = connectionFactory1.createConnection();
                Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageConsumer messageConsumer = session.createConsumer(queue1);
                connection.start();
                while (true) {
                    TextMessage textMessage = (TextMessage)messageConsumer.receive(1);
                    if (textMessage != null) {
                        System.out.println(textMessage.getText());
                        PutMessageInSecondQueue(textMessage);
                    }
                    else {
                         break;
                    }
                }
                
                messageConsumer.close();
                connection.close();                
            }
            catch (JMSException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);                
            }
    }
    
    private void PutMessageInSecondQueue(TextMessage message){
            try {
                Connection connection = connectionFactory2.createConnection();
                Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageProducer messageProducer = session.createProducer(queue2);
                
                TextMessage textMessage = session.createTextMessage();
                textMessage.setText("Hello ".concat(message.getText()));
                
                messageProducer.send(textMessage);
                messageProducer.close();
                connection.close();
            }
            catch (JMSException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
    }
}
