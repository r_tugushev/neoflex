/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainpackage.servlets;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author rushan
 */
@WebServlet(name = "MyServlet", urlPatterns = {"/MyServlet"})
public class MyServlet extends HttpServlet {
    @Resource(mappedName="jms/ConnectionFactory1")
    private ConnectionFactory connectionFactory1;

    @Resource(mappedName="jms/Destination1")
    private Queue queue1;

    @Resource(mappedName="jms/ConnectionFactory2")
    private ConnectionFactory connectionFactory2;

    @Resource(mappedName="jms/Destination2")
    private Queue queue2;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String strMessage = request.getParameter("message");
        String processedMessage = null;
        AddMessageToQueue(strMessage);
        GetMessageFromQueue(processedMessage);
        
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Пошлите сообщение в очередь</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Отправление сообщений</h1>");
            
            // The following code adds the form to the web page
            out.println("<form method=\"post\" >");            
            out.println("Message: <input type='text' name='message'><br/>");
            out.println("</br>");                                    
            out.println("<input type='submit'><br/>");
            out.println("</form>");                        
            out.println("<div>");
            if(processedMessage != null)out.println();
            out.println("</div>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    private void GetMessageFromQueue(String message) {

        try {
            Connection connection = connectionFactory2.createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageConsumer messageConsumer = session.createConsumer(queue2);
            connection.start();
            while (true) {
                TextMessage textMessage = (TextMessage)messageConsumer.receive(1);
                if (textMessage != null) {
                    message = textMessage.getText();
                }
                else {
                    break;
                }
            }
            
            messageConsumer.close();
            connection.close();
        }
        catch (JMSException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void AddMessageToQueue(String strMessage) {
        if (strMessage!=null && !"".equals(strMessage.trim())) {
            try {
                Connection connection = connectionFactory1.createConnection();
                Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageProducer messageProducer = session.createProducer(queue1);
                
                TextMessage textMessage = session.createTextMessage();
                textMessage.setText(strMessage);
                
                messageProducer.send(textMessage);
                messageProducer.close();
                connection.close();
            }
            catch (JMSException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
